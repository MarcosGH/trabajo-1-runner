using UnityEngine;

public class ScaleCycle : MonoBehaviour
{
    public float scaleSpeed = 1.0f;    // Velocidad de cambio de escala
    public float minScale = 0.5f;      // Escala m�nima
    public float maxScale = 2.0f;      // Escala m�xima

    private void Update()
    {
        // Calcula el factor de escala basado en una funci�n sinusoidal
        float scaleFactor = Mathf.Sin(Time.time * scaleSpeed);

        // Mapea el factor de escala (que oscila entre -1 y 1) al rango entre minScale y maxScale
        float scaledValue = Mathf.Lerp(minScale, maxScale, (scaleFactor + 1f) / 2f);

        // Aplica la escala al objeto
        transform.localScale = new Vector3(scaledValue, scaledValue, scaledValue);
    }
}