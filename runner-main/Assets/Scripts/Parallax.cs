﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;
    public float speedMultiplier = 1.0f; // Multiplicador de velocidad del efecto paralax

    private bool isParallaxActive = true;

    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        if (isParallaxActive)
        {
            // Aplica el desplazamiento del efecto paralax multiplicado por el speedMultiplier
            float horizontalMovement = parallaxEffect * speedMultiplier * Time.deltaTime;
            transform.position = new Vector3(transform.position.x - horizontalMovement, transform.position.y, transform.position.z);

            if (transform.localPosition.x < -20)
            {
                transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
            }
        }
    }

    public void StopParallax()
    {
        // Metodo para detener el efecto paralax
        isParallaxActive = false;
    }
}